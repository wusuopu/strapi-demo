'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async check (ctx) {
    console.log('nursery check:', ctx.state)
    ctx.send({
      success: true
    })
  },
};
