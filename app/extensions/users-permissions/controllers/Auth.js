'use strict';

const axios = require('axios')

const formatError = error => [
  { messages: [{ id: error.id, message: error.message, field: error.field }] },
];

module.exports = {
  async callback(ctx) {
    // The identifier is required.
    const params = ctx.request.body;
    if (!params.identifier) {
      return ctx.badRequest(
        null,
        formatError({
          id: 'Auth.form.error.email.provide',
          message: 'Please provide your username or your e-mail.',
        })
      );
    }
    // The password is required.
    if (!params.password) {
      return ctx.badRequest(
        null,
        formatError({
          id: 'Auth.form.error.password.provide',
          message: 'Please provide your password.',
        })
      );
    }

    try {
      const request = axios.create({
        baseURL: 'http://backend:3000'
      })
      console.log('user login:', params)
      // login
      let res = await request.post(
        '/api/v1/users/auth/login',
        {email: params.identifier, password: params.password, keep_login: true}
      )
      const token = res.data.data.attributes.token
      const uid = res.data.data.id
      request.defaults.headers.common.Authorization = `token ${token}`

      // user info
      res = await request.get(`/api/v1/users/${uid}`)
      const info = res.data.data.attributes

      ctx.send({
        jwt: strapi.plugins['users-permissions'].services.jwt.issue({
          id: uid, token,
        }),
        user: {
          id: uid,
          username: info.name,
          email: info.email,
          provider: 'local',
          confirmed: true,
          blocked: false,
          role: {id: 1, name: 'Authenticated', type: 'authenticated'},
        },
      });
    } catch (e) {
      return ctx.badRequest(
        null,
        formatError({
          id: 'Auth.form.error.invalid',
          message: 'Identifier or password invalid.',
        })
      );
    }
  },
};
