'use strict';

const axios = require('axios')

module.exports = strapi => ({
  initialize() {
    const passportMiddleware = strapi.admin.services.passport.init();

    strapi.app.use(passportMiddleware);

    strapi.app.use(async (ctx, next) => {
      if (
        ctx.request.header.authorization &&
        ctx.request.header.authorization.split(' ')[0] === 'Bearer'
      ) {
        const token = ctx.request.header.authorization.split(' ')[1];

        const { payload, isValid } = strapi.admin.services.token.decodeJwtToken(token);
        const remoteProviderRole = {id: 2, name: 'Editor', code: 'strapi-editor'}
        if (isValid) {
          // request is made by an admin
          let admin
          if (payload.provider === 'remote') {
            // use remote account
            const request = axios.create({
              baseURL: 'http://backend:3000'
            })
            const token = payload.token
            request.defaults.headers.common.Authorization = `token ${token}`
            const res = await request.get(`/api/v1/admins/administrators/${payload.id}`)
            admin = {
              id: payload.id,
              email: res.data.data.attributes.email,
              isActive: true,
              blocked: false,
              token,
              provider: payload.provider,
              roles: [remoteProviderRole],
            }
          } else {
            // use local account
            admin = await strapi.query('user', 'admin').findOne({ id: payload.id }, ['roles']);
          }
          if (!admin || !(admin.isActive === true)) {
            return ctx.forbidden('Invalid credentials');
          }

          ctx.state.admin = admin;
          ctx.state.user = admin;
          ctx.state.userAbility = await strapi.admin.services.permission.engine.generateUserAbility(
            admin
          );
          ctx.state.isAuthenticatedAdmin = true;
          console.log('auth middleware:', payload)
          return next();
        }
      }

      return next();
    });
  },
});
