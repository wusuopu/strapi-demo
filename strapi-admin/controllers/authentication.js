'use strict';

const passport = require('koa-passport');
const compose = require('koa-compose');
const axios = require('axios')

const {
  validateRegistrationInput,
  validateAdminRegistrationInput,
  validateRegistrationInfoQuery,
  validateForgotPasswordInput,
  validateResetPasswordInput,
} = require('../validation/authentication');

const passportAuth = (ctx, next) => {
  return passport.authenticate('local', { session: false }, (err, user, info) => {
    if (err) {
      strapi.eventHub.emit('admin.auth.error', { error: err, provider: 'local' });
      return ctx.badImplementation();
    }

    if (!user) {
      strapi.eventHub.emit('admin.auth.error', {
        error: new Error(info.message),
        provider: 'local',
      });
      return ctx.badRequest(info.message);
    }

    ctx.state.user = user;

    strapi.eventHub.emit('admin.auth.success', { user, provider: 'local' });

    return next();
  })(ctx, next);
}
const remoteAuth = async (ctx, next) => {
  const params = ctx.request.body;
  const request = axios.create({
    baseURL: 'http://backend:3000'
  })
  const provider = 'remote'
  // login
  try {
    let res = await request.post(
      '/api/v1/admins/auth/login',
      {email: params.email, password: params.password, keep_login: true}
    )
    const token = res.data.data.attributes.token
    const uid = res.data.data.id

    const user = {
      id: uid,
      email: params.email,
      isActive: true,
      blocked: false,
      token,
      provider,
      roles: [{id: 2, name: 'Editor', code: 'strapi-editor'}],
    }
    ctx.state.user = user
    strapi.eventHub.emit('admin.auth.success', { user, provider });
  } catch (e) {
    strapi.eventHub.emit('admin.auth.error', { error: e, provider });
    return ctx.badRequest();
  }
  next()
}

module.exports = {
  login: compose([
    (ctx, next) => {
      if (ctx.request.body.provider === 'remote') {
        return remoteAuth(ctx, next)
      }
      return passportAuth(ctx, next)
    },
    ctx => {
      const { user } = ctx.state;
      const token = strapi.admin.services.token.createJwtToken(user)

      console.log('admin login:', user, token)

      ctx.body = {
        data: {
          token,
          user: strapi.admin.services.user.sanitizeUser(ctx.state.user), // TODO: fetch more detailed info
        },
      };
    },
  ]),

  renewToken(ctx) {
    const { token } = ctx.request.body;

    if (token === undefined) {
      return ctx.badRequest('Missing token');
    }

    const { isValid, payload } = strapi.admin.services.token.decodeJwtToken(token);

    if (!isValid) {
      return ctx.badRequest('Invalid token');
    }

    ctx.body = {
      data: {
        token: strapi.admin.services.token.createJwtToken({ id: payload.id }),
      },
    };
  },

  async registrationInfo(ctx) {
    try {
      await validateRegistrationInfoQuery(ctx.request.query);
    } catch (err) {
      return ctx.badRequest('QueryError', err);
    }

    const { registrationToken } = ctx.request.query;

    const registrationInfo = await strapi.admin.services.user.findRegistrationInfo(
      registrationToken
    );

    if (!registrationInfo) {
      return ctx.badRequest('Invalid registrationToken');
    }

    ctx.body = { data: registrationInfo };
  },

  async register(ctx) {
    const input = ctx.request.body;

    try {
      await validateRegistrationInput(input);
    } catch (err) {
      return ctx.badRequest('ValidationError', err);
    }

    const user = await strapi.admin.services.user.register(input);

    ctx.body = {
      data: {
        token: strapi.admin.services.token.createJwtToken(user),
        user: strapi.admin.services.user.sanitizeUser(user),
      },
    };
  },

  async registerAdmin(ctx) {
    const input = ctx.request.body;

    try {
      await validateAdminRegistrationInput(input);
    } catch (err) {
      return ctx.badRequest('ValidationError', err);
    }

    const hasAdmin = await strapi.admin.services.user.exists();

    if (hasAdmin) {
      return ctx.badRequest('You cannot register a new super admin');
    }

    const superAdminRole = await strapi.admin.services.role.getSuperAdmin();

    if (!superAdminRole) {
      throw new Error(
        "Cannot register the first admin because the super admin role doesn't exist."
      );
    }

    const user = await strapi.admin.services.user.create({
      ...input,
      registrationToken: null,
      isActive: true,
      roles: superAdminRole ? [superAdminRole.id] : [],
    });

    await strapi.telemetry.send('didCreateFirstAdmin');

    ctx.body = {
      data: {
        token: strapi.admin.services.token.createJwtToken(user),
        user: strapi.admin.services.user.sanitizeUser(user),
      },
    };
  },

  async forgotPassword(ctx) {
    const input = ctx.request.body;

    try {
      await validateForgotPasswordInput(input);
    } catch (err) {
      return ctx.badRequest('ValidationError', err);
    }

    strapi.admin.services.auth.forgotPassword(input);

    ctx.status = 204;
  },

  async resetPassword(ctx) {
    const input = ctx.request.body;

    try {
      await validateResetPasswordInput(input);
    } catch (err) {
      return ctx.badRequest('ValidationError', err);
    }

    const user = await strapi.admin.services.auth.resetPassword(input);

    ctx.body = {
      data: {
        token: strapi.admin.services.token.createJwtToken(user),
        user: strapi.admin.services.user.sanitizeUser(user),
      },
    };
  },
};
